# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
- First release



[0.0.4]: ../../branches/compare/v0.0.4..v0.0.3#diff
[0.0.3]: ../../branches/compare/v0.0.3..v0.0.2#diff
[0.0.2]: ../../branches/compare/v0.0.2..v0.0.1#diff
[0.0.1]: ../../src/v0.0.1
[Unreleased]: ../../src
