# gradle-buildinfo-plugin

[![Maven version](https://img.shields.io/maven-metadata/v/https/plugins.gradle.org/m2/com/mreil/buildinfo/com.mreil.buildinfo.gradle.plugin/maven-metadata.xml.svg)](https://plugins.gradle.org/plugin/com.mreil.buildinfo)
[![Build status](https://img.shields.io/bitbucket/pipelines/mreil-com/gradle-buildinfo-plugin.svg)](https://bitbucket.org/mreil-com/gradle-buildinfo-plugin)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> Collect build time info for a Gradle project

## Install

Install the plugin as follows.

Only install the plugin in the root module of your project.

Use at least Gradle 4.6.

```
#!groovy

plugins {
    id "com.mreil.buildinfo" version "0.0.1"
}

```


## Usage

### Configuration

The plugin can be configured as follows:

```
#!groovy

buildinfo {
    outputFile = "build/buildinfo/buildinfo.json"
}

```


### Tasks

#### buildinfo

Shows all configured default versions for dependencies.



## Changes

See [CHANGELOG.md][changelog] for changes.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


## Contribute

Please raise issues [here](../../issues?status=new&status=open).

Feel free to address existing issues and raise a [pull request](../../pull-requests).


## License

[MIT][license]


[changelog]: CHANGELOG.md
[license]: LICENSE
