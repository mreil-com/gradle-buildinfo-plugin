package com.mreil.gradleplugins.buildinfo

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

class BuildinfoTask extends DefaultTask {
    public static final String NAME = "buildinfo"
    private static final ObjectWriter OM = new ObjectMapper()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .registerModules(new JavaTimeModule())
            .writerWithDefaultPrettyPrinter()

    @TaskAction
    def task() {
        BuildinfoExtension ext = project.extensions.getByName(BuildinfoExtension.NAME) as BuildinfoExtension

        def provider = new BuildInfoProvider(project.getProjectDir())
        def info = provider.getInfo()
        ext.buildinfo = info

        def jsonString = OM.writeValueAsString(info)
        writeToFile(ext, jsonString)
    }

    private writeToFile(BuildinfoExtension ext, String jsonString) {
        def file = getOutputFile(ext)
        file.parentFile.mkdirs()
        file.write(jsonString)
    }

    private File getOutputFile(BuildinfoExtension extension) {
        return extension.outputFile
    }
}
