package com.mreil.gradleplugins.buildinfo

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.bundling.AbstractArchiveTask
import org.gradle.jvm.tasks.Jar

import java.nio.file.Paths

class BuildinfoPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        if (project.rootProject != project) {
            throw new GradleException("Plugin must be configured on root project")
        }

        project.plugins.apply("base")

        def ext = project.extensions.create(BuildinfoExtension.NAME,
                BuildinfoExtension,
                project) as BuildinfoExtension

        def task = project.tasks.create(BuildinfoTask.NAME, BuildinfoTask) as BuildinfoTask

        project.allprojects*.afterEvaluate { Project p ->
            p.with {
                tasks.findAll { isTaskA(it, AbstractArchiveTask) }
                        .each { AbstractArchiveTask archiveTask ->
                    logger.info("Adding buildinfo output file ${ext.outputFile} to artifact of task ${archiveTask}")
                    archiveTask.dependsOn(task)
                    addFileToArtifact(archiveTask, ext)
                }
            }
        }
    }

    private addFileToArtifact(AbstractArchiveTask archiveTask,
                              BuildinfoExtension ext) {
        archiveTask.from(ext.outputFile.parent) {
            def outDir = ext.artifactDir

            // if task is Jar task, use META-INF directory
            if (isTaskA(archiveTask, Jar)) {
                outDir = Paths.get("/META-INF").resolve(outDir).toString()
            }

            into outDir
        }
    }

    /**
     * Check if <i>task</i> is an instance of <i>test</i>.
     *
     * @param task the task to be tested
     * @param test the task class to be tested for
     * @return
     */
    private boolean isTaskA(Task task, Class<? extends Task> test) {
        return test.isAssignableFrom(task.class)
    }
}
