package com.mreil.gradleplugins.buildinfo

import org.gradle.api.Project

class BuildinfoExtension {
    public static final String NAME = "buildinfo"
    private final Project project

    private Buildinfo buildinfo

    File outputFile = new File(project.buildDir, "buildinfo/buildinfo.json")
    String artifactDir = "buildinfo"

    BuildinfoExtension(Project project) {
        this.project = project
    }

    void setBuildinfo(Buildinfo buildinfo) {
        this.buildinfo = buildinfo
    }

    Buildinfo getBuildinfo() {
        return buildinfo
    }
}
