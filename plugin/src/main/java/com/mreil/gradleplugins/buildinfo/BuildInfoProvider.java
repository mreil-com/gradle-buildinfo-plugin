package com.mreil.gradleplugins.buildinfo;

import com.google.common.collect.ImmutableList;
import com.mreil.gradleplugins.buildinfo.buildnumber.BuildNumberProvider;
import com.mreil.gradleplugins.buildinfo.ci.CiProvider;
import com.mreil.gradleplugins.buildinfo.date.BuildDateProvider;
import com.mreil.gradleplugins.buildinfo.vcs.VcsInfoProvider;
import lombok.SneakyThrows;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Provides build info for a build.
 */
public class BuildInfoProvider {
    private final Map<BuildInfoFragmentProvider, Object> info;

    /**
     * Collect all available build info.
     *
     * @param projectDirectory the project root directory
     */
    public BuildInfoProvider(File projectDirectory) {
        ImmutableList.Builder<BuildInfoFragmentProvider> providers =
                new ImmutableList.Builder<BuildInfoFragmentProvider>()
                        .add(new BuildDateProvider(projectDirectory))
                        .add(new BuildNumberProvider(projectDirectory))
                        .add(new CiProvider(projectDirectory));

        VcsInfoProvider.getValid(projectDirectory).ifPresent(providers::add);

        info = providers.build().stream()
                .collect(Collectors.toMap(prov -> prov,
                        BuildInfoFragmentProvider::get))
                .entrySet().stream()
                .filter(entry -> entry.getValue().isPresent())
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().get()));
    }

    /**
     * Get build info.
     *
     * @return the build info
     */
    public Buildinfo getInfo() {
        return Buildinfo.from(info);
    }

    public static boolean isValidVcsProvider(BuildInfoFragmentProvider provider) {
        return !VcsInfoProvider.class.isAssignableFrom(provider.getClass())
                || ((VcsInfoProvider) provider).isValid();
    }

    @SneakyThrows
    public static BuildInfoFragmentProvider newInstance(
            File projectDirectory,
            Constructor<? extends BuildInfoFragmentProvider> cons) {
        return cons.newInstance(projectDirectory);
    }

    @SneakyThrows
    public static Constructor<? extends BuildInfoFragmentProvider> getConstructor(
            Class<? extends BuildInfoFragmentProvider> provider) {
        return provider.getConstructor(File.class);
    }
}
