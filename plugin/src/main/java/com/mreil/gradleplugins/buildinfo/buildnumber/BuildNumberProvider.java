package com.mreil.gradleplugins.buildinfo.buildnumber;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.mreil.gradleplugins.buildinfo.AbstractBuildInfoFragmentProvider;
import com.mreil.gradleplugins.buildinfo.util.EnvVariableReader;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Gets the build number from the build environment.
 * <p>
 * Works for the following CI systems:
 * <ul>
 * <li>Bamboo</li>
 * <li>Bitbucket</li>
 * <li>Circle CI</li>
 * <li>Jenkins</li>
 * </ul>
 */
public class BuildNumberProvider extends AbstractBuildInfoFragmentProvider<Integer> {
    private static final List<String> KEYS = ImmutableList.of(
            "BITBUCKET_BUILD_NUMBER", // Bitbucket Cloud
            "bamboo.buildNumber",        // Bamboo
            "CIRCLE_BUILD_NUM",         // Circle CI
            "BUILD_NUMBER"               // Jenkins
    );
    private final EnvVariableReader reader;

    public BuildNumberProvider(File projectDirectory) {
        super(projectDirectory);
        reader = new EnvVariableReader();
    }

    @VisibleForTesting
    BuildNumberProvider(EnvVariableReader reader) {
        super(null);
        this.reader = reader;
    }

    @Override
    public Optional<Integer> get() {
        return reader.findFirst(KEYS)
                .map(Integer::parseInt);
    }

    @Override
    public String getName() {
        return "buildNumber";
    }
}
