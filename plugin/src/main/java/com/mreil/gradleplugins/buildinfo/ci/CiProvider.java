package com.mreil.gradleplugins.buildinfo.ci;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.mreil.gradleplugins.buildinfo.AbstractBuildInfoFragmentProvider;
import com.mreil.gradleplugins.buildinfo.util.EnvVariableReader;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Detects a CI environment.
 */
public class CiProvider extends AbstractBuildInfoFragmentProvider<Boolean> {
    private static final List<String> KEYS = ImmutableList.of(
            "CI",                       // Bitbucket, CircleCi
            "bamboo.buildNumber",   // Bamboo
            "BUILD_NUMBER"          // Jenkins
    );
    private final EnvVariableReader reader;

    public CiProvider(File projectDirectory) {
        super(projectDirectory);
        this.reader = new EnvVariableReader();
    }

    @VisibleForTesting
    protected CiProvider(EnvVariableReader reader) {
        super(null);
        this.reader = reader;
    }

    @Override
    public Optional<Boolean> get() {
        return Optional.of(reader.findFirst(KEYS).isPresent());
    }

    @Override
    public String getName() {
        return "ciBuild";
    }
}
