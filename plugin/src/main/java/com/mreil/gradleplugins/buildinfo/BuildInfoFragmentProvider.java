package com.mreil.gradleplugins.buildinfo;

import java.util.Optional;

/**
 * Provides one fragment (i.e. key-value pair) of build info.
 */
public interface BuildInfoFragmentProvider<T> {
    Optional<T> get();

    String getName();
}
