package com.mreil.gradleplugins.buildinfo.vcs;

import com.mreil.gradleplugins.buildinfo.BuildInfoFragmentProvider;
import com.mreil.gradleplugins.buildinfo.ClassScanner;

import java.io.File;
import java.util.Optional;
import java.util.Set;

/**
 * Can retrieve an instance of {@link VcsInfo} for a given source control system.
 */
public interface VcsInfoProvider extends BuildInfoFragmentProvider<VcsInfo> {
    boolean isValid();

    boolean validate();

    static Set<Class<? extends VcsInfoProvider>> findProviders() {
        return new ClassScanner<VcsInfoProvider>().findAll(VcsInfoProvider.class);
    }

    /**
     * Finds a valid VCS info provider.
     */
    static Optional<? extends VcsInfoProvider> getValid(File dir) {
        return findProviders().stream()
                .map(VcsUtils::getConstructor)
                .map(cons -> VcsUtils.newInstance(dir, cons))
                .filter(VcsInfoProvider::isValid)
                .findFirst();
    }
}
