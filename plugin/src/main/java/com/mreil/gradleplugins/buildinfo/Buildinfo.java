package com.mreil.gradleplugins.buildinfo;

import com.mreil.gradleplugins.buildinfo.vcs.VcsInfo;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.time.ZonedDateTime;
import java.util.Map;

/**
 * Build info.
 * <p>
 * Contains all info about a build, e.g. version control info, build number, etc.
 */
@Data
final class Buildinfo {
    private ZonedDateTime buildDate;
    private Boolean ciBuild;
    private VcsInfo versionControl;
    private Integer buildNumber;

    private Buildinfo() {
    }


    static Buildinfo from(Map<? extends BuildInfoFragmentProvider, Object> all) {
        final Buildinfo info = new Buildinfo();

        for (Map.Entry<? extends BuildInfoFragmentProvider, Object> entry : all.entrySet()) {
            try {
                final Method method = Buildinfo.class.getDeclaredMethod(setterFor(entry),
                        entry.getValue().getClass());
                method.invoke(info, entry.getValue());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return info;
    }

    private static String setterFor(Map.Entry<? extends BuildInfoFragmentProvider, Object> entry) {
        return "set" + StringUtils.capitalize(entry.getKey().getName());
    }
}
