package com.mreil.gradleplugins.buildinfo.vcs;

import lombok.SneakyThrows;

import java.io.File;
import java.lang.reflect.Constructor;

/**
 * Utility functions to create vcs providers.
 */
final class VcsUtils {
    private VcsUtils() {
    }

    @SneakyThrows
    static VcsInfoProvider newInstance(
            File projectDirectory,
            Constructor<? extends VcsInfoProvider> cons) {
        return cons.newInstance(projectDirectory);
    }

    @SneakyThrows
    static Constructor<? extends VcsInfoProvider> getConstructor(
            Class<? extends VcsInfoProvider> provider) {
        return provider.getConstructor(File.class);
    }
}
