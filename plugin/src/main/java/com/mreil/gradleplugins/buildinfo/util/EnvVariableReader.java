package com.mreil.gradleplugins.buildinfo.util;

import com.google.common.annotations.VisibleForTesting;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.empty;

/**
 * Reads variables from the environment.
 */
public class EnvVariableReader {
    private final Map<String, String> env;

    /**
     * Lazy holder for env variables.
     */
    private static class LazyHolder {
        private static final Map<String, String> INSTANCE = System.getenv();
    }

    @VisibleForTesting
    protected EnvVariableReader(Map<String, String> env) {
        this.env = env;
    }

    public EnvVariableReader() {
        this.env = LazyHolder.INSTANCE;
    }

    /**
     * Find the value of the first variable found from a list of keys.
     *
     * @param keys a list of variable names
     * @return the value of the first key found
     */
    public Optional<String> findFirst(List<String> keys) {
        if (keys == null) {
            return empty();
        }

        return keys.stream()
                .map(env::get)
                .filter(Objects::nonNull)
                .findFirst();
    }
}
