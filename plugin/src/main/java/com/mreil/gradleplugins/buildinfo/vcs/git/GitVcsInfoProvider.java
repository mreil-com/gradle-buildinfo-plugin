package com.mreil.gradleplugins.buildinfo.vcs.git;

import com.mreil.gradleplugins.buildinfo.vcs.AbstractVcsInfoProvider;
import com.mreil.gradleplugins.buildinfo.vcs.VcsInfo;
import com.mreil.gradleplugins.buildinfo.vcs.VcsType;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.eclipse.jgit.lib.Constants.HEAD;

/**
 * VCS provider for Git.
 */
@Slf4j
public class GitVcsInfoProvider extends AbstractVcsInfoProvider {
    private Git git;

    public GitVcsInfoProvider(File dir) {
        super(dir);
    }

    @Override
    public String getType() {
        return "git";
    }

    @Override
    public boolean validate() {
        File dir = this.projectDirectory;
        while (this.git == null && dir != null) {
            git = getGitDirAt(dir);
            if (git == null) {
                dir = dir.getParentFile();
            } else {
                return true;
            }
        }
        return false;
    }

    private Git getGitDirAt(File dir) {
        try {
            return Git.open(dir);
        } catch (IOException e) {
            log.info("No git directory: " + dir + ", " + e.getMessage());
            return null;
        }
    }

    @Override
    public Optional<VcsInfo> doGet() {
        final Ref head = getHead();
        if (head.getObjectId() == null) {
            // git dir initialized, but no commits
            return Optional.empty();
        }

        return Optional.ofNullable(
                VcsInfo.builder()
                        .type(VcsType.GIT)
                        .revision(head.getObjectId().getName())
                        .shortRevision(head.getObjectId().abbreviate(6).name())
                        .branch(getBranch())
                        .tags(getTags(head))
                        .build());
    }

    private List<String> getTags(Ref head) {
        final List<Ref> list = getAllTags();
        final List<String> tags = new ArrayList<>();
        for (Ref tag : list) {
            if (refEqualsTag(head, tag)) {
                tags.add(tag.getName().replaceFirst("^refs/tags/", ""));
            }
        }
        return tags;
    }

    private boolean refEqualsTag(Ref head, Ref tag) {
        if (Objects.equals(head.getObjectId(), tag.getObjectId())) {
            return true;
        }

        try {
            final Ref peeled = git.getRepository().getRefDatabase().peel(tag);
            return Objects.equals(head.getObjectId(), peeled.getPeeledObjectId());
        } catch (IOException e) {
            return false;
        }

    }

    private List<Ref> getAllTags() {
        try {
            return git.tagList().call();
        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
    }

    private String getBranch() {
        try {
            return getRepository().getBranch();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Ref getHead() {
        try {
            return getRepository().exactRef(HEAD);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Repository getRepository() {
        if (git == null) {
            throw new IllegalStateException("git not set, call isVcs() to check if it's a valid repo");
        } else {
            return git.getRepository();
        }
    }
}
