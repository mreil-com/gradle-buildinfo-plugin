package com.mreil.gradleplugins.buildinfo;

import java.io.File;

/**
 * Superclass for all fragment providers.
 */
public abstract class AbstractBuildInfoFragmentProvider<T> implements BuildInfoFragmentProvider<T> {
    protected final File projectDirectory;

    public AbstractBuildInfoFragmentProvider(File projectDirectory) {
        this.projectDirectory = projectDirectory;
    }
}
