package com.mreil.gradleplugins.buildinfo;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import lombok.SneakyThrows;

import java.lang.reflect.Modifier;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Finds all implementations of a class.
 */
@SuppressWarnings({"UnstableApiUsage", "unchecked"})
public class ClassScanner<T> {
    /**
     * Find all subclasses of <i>clz</i>.
     *
     * @param clz a class
     * @return the set of subclasses of <i>clz</i>
     */
    @SneakyThrows
    public Set<Class<? extends T>> findAll(Class<T> clz) {
        final ClassPath clp = ClassPath.from(Thread.currentThread().getContextClassLoader());

        return getAllClassesInPackage(clp, clz).stream()
                .map(this::loadClass)
                .filter(clazz -> !clazz.isInterface())
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()))
                .filter(clz::isAssignableFrom)
                .collect(Collectors.toSet());
    }

    private Class<T> loadClass(ClassPath.ClassInfo clazz) {
        return (Class<T>) clazz.load();
    }

    private static ImmutableSet<ClassPath.ClassInfo> getAllClassesInPackage(ClassPath clp, Class clz) {
        return clp.getTopLevelClassesRecursive(clz.getPackage().getName());
    }
}
