package com.mreil.gradleplugins.buildinfo.date;

import com.mreil.gradleplugins.buildinfo.AbstractBuildInfoFragmentProvider;

import java.io.File;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Gets the current date.
 * <p>
 * Date is in UTC zone.
 */
public class BuildDateProvider extends AbstractBuildInfoFragmentProvider<ZonedDateTime> {
    public BuildDateProvider(File projectDirectory) {
        super(projectDirectory);
    }

    @Override
    public Optional<ZonedDateTime> get() {
        return Optional.of(ZonedDateTime.now(ZoneOffset.UTC));
    }

    @Override
    public String getName() {
        return "buildDate";
    }
}
