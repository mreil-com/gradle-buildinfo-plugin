package com.mreil.gradleplugins.buildinfo.vcs;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean for VCS info.
 */
@Data
@Builder
public class VcsInfo {
    @NonNull
    private VcsType type;

    @NonNull
    private String revision;

    @NonNull
    private String shortRevision;

    @Builder.Default
    private List<String> tags = new ArrayList<>();

    private String branch;
}
