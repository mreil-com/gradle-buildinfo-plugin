package com.mreil.gradleplugins.buildinfo.vcs;

import com.mreil.gradleplugins.buildinfo.AbstractBuildInfoFragmentProvider;

import java.io.File;
import java.util.Optional;

/**
 * Superclass for all VCS providers.
 */
public abstract class AbstractVcsInfoProvider extends AbstractBuildInfoFragmentProvider<VcsInfo>
        implements VcsInfoProvider {

    private final boolean valid;

    protected AbstractVcsInfoProvider(File projectDir) {
        super(projectDir);

        if (!projectDirectory.toPath().isAbsolute()) {
            throw new IllegalArgumentException("Path must be absolute: " + projectDirectory);
        }

        if (!projectDirectory.exists()) {
            throw new IllegalArgumentException("Directory does not exist: " + projectDirectory);
        }

        valid = validate();
    }

    @Override
    public final boolean isValid() {
        return valid;
    }

    @Override
    public String getName() {
        return "versionControl";
    }

    public abstract String getType();

    @Override
    public final Optional<VcsInfo> get() {
        if (valid) {
            return doGet();
        } else {
            throw new RuntimeException("not valid");
        }
    }

    protected abstract Optional<VcsInfo> doGet();
}
