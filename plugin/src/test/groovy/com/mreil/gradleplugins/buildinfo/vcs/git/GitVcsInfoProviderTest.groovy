package com.mreil.gradleplugins.buildinfo.vcs.git

import org.eclipse.jgit.api.Git
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class GitVcsInfoProviderTest extends Specification {
    @Rule
    TemporaryFolder tmp = new TemporaryFolder()

    def "No git dir is not valid"() {
        expect:
            !new GitVcsInfoProvider(tmp.root).valid
    }

    def "git dir without commits is valid"() {
        when:
            Git.init().setDirectory(tmp.root).call()

        then:
            def provider = new GitVcsInfoProvider(tmp.root)
            provider.valid
            !provider.get().present
    }

    def "git dir with commits is valid"() {
        when:
            initRepo()

        then:
            def provider = new GitVcsInfoProvider(tmp.root)
            provider.valid
            provider.get().present
    }

    Git initRepo() {
        def git = Git.init().setDirectory(tmp.root).call()
        tmp.newFile("meh").write("bla")
        git.add().addFilepattern("meh").call()
        git.commit().setMessage("blabla").call()
        return git
    }

    def "subdirectory is valid"() {
        when:
            initRepo()
            def subDir = tmp.newFolder("subdir")

        then:
            def provider = new GitVcsInfoProvider(subDir)
            provider.valid
            provider.get().present
    }

    def "with tag"() {
        when:
            def git = Git.init().setDirectory(tmp.root).call()
            def file = tmp.newFile("meh")
            git.add().addFilepattern(file.name).call()
            git.commit().setMessage("commit").call()
            git.tag().setName("t1").setAnnotated(false).call()

        then:
            def info = new GitVcsInfoProvider(tmp.root).get()
            info.present
            info.ifPresent {
                assert it.revision
                assert it.shortRevision
                assert it.branch == "master"
                assert it.tags == ["t1"]
            }
    }

    def "with tag annotated"() {
        when:
            def git = Git.init().setDirectory(tmp.root).call()
            def file = tmp.newFile("meh")
            git.add().addFilepattern(file.name).call()
            git.commit().setMessage("commit").call()
            git.tag().setName("t2").setMessage("m2").setAnnotated(true).call()

        then:
            def info = new GitVcsInfoProvider(tmp.root).get()
            info.present
            info.ifPresent {
                assert it.revision
                assert it.shortRevision
                assert it.branch == "master"
                assert it.tags == ["t2"]
            }
    }

    def "only tag on HEAD"() {
        when:
            def git = Git.init().setDirectory(tmp.root).call()
            // 1st commit and tag
            def file = tmp.newFile("meh")
            git.add().addFilepattern(file.name).call()
            git.commit().setMessage("commit").call()
            git.tag().setName("t1").setAnnotated(false).call()
            // 2nd commit and tag
            file = tmp.newFile("meh2")
            git.add().addFilepattern(file.name).call()
            git.commit().setMessage("commit").call()
            git.tag().setName("t2").setAnnotated(false).call()

        then:
            def info = new GitVcsInfoProvider(tmp.root).get()
            info.present
            info.ifPresent {
                assert it.revision
                assert it.shortRevision
                assert it.branch == "master"
                assert it.tags == ["t2"]
            }
    }

    def "with existing config directory"() {
        when:
            tmp.root.toPath().resolve("config").toFile().mkdir()

        then:
            new GitVcsInfoProvider(tmp.root)
    }

}
