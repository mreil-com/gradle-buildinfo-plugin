package com.mreil.gradleplugins.buildinfo

import org.eclipse.jgit.api.Git
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import java.time.ZonedDateTime

class BuildInfoProviderTest extends Specification {

    @Rule
    TemporaryFolder tmp = new TemporaryFolder()

    def "test provider"() {
        when:
            def provider = new BuildInfoProvider(tmp.root)

        then:
            def info = provider.getInfo()
            info != null
            // is "false" locally, but "true" in Pipelines
            info.ciBuild != null
            info.buildDate instanceof ZonedDateTime
    }

    def "test vcs provider"() {
        setup:
            Git git = Git.init().setDirectory(tmp.root).call()
            tmp.newFile("meh").write("bla")
            git.add().addFilepattern("meh").call()
            git.commit().setMessage("blabla").call()

        when:
            def provider = new BuildInfoProvider(tmp.root)

        then:
            def info = provider.getInfo()
            info.versionControl != null
    }
}
