package com.mreil.gradleplugins.buildinfo

import com.fasterxml.jackson.databind.util.ISO8601DateFormat
import groovy.json.JsonSlurper
import org.gradle.api.Project
import org.gradle.api.internal.plugins.PluginApplicationException
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class BuildinfoPluginTest extends Specification {

    @Rule
    TemporaryFolder tmp = new TemporaryFolder()

    def "Plugin can be applied"() {
        when:
            def gradle = buildParentProject()
            gradle.plugins.apply(BuildinfoPlugin)

        then:
            runTask(gradle, "buildinfo")
    }

    def "Plugin can't be applied in child"() {
        setup:
            def parent = buildParentProject()
            def child = buildChildProject(parent)

        when:
            child.plugins.apply(BuildinfoPlugin)

        then:
            thrown PluginApplicationException
    }

    def "Plugin can be applied in parent"() {
        when:
            def parent = buildParentProject()
            def child = buildChildProject(parent)
            parent.plugins.apply(BuildinfoPlugin)

        then:
            def task = parent.tasks.getByName("buildinfo")
            task.actions.each {
                it.execute(task)
            }
    }

    def "buildinfo file is created in single module project"() {
        when:
            def parent = buildParentProject()
            parent.plugins.apply(BuildinfoPlugin)
            runTask(parent, "buildinfo")

        then:
            new File(parent.getBuildDir(), "buildinfo/buildinfo.json").exists()
    }

    def "buildinfo file is created in multi module project"() {
        when:
            def parent = buildParentProject()
            def child = buildChildProject(parent)
            parent.plugins.apply(BuildinfoPlugin)
            runTask(parent, "buildinfo")

        then:
            new File(parent.getBuildDir(), "buildinfo/buildinfo.json").exists()
    }

    def "buildinfo file contains JSON"() {
        when:
            def parent = buildParentProject()
            parent.plugins.apply(BuildinfoPlugin)
            runTask(parent, "buildinfo")

        then:
            def json = new JsonSlurper().parse(parent.buildinfo.outputFile)
            def date = json["buildDate"]
            date != null
            new ISO8601DateFormat().parse(date)
    }

    def "buildinfo JSON does not contain null fields"() {
        when:
            def parent = buildParentProject()
            parent.plugins.apply(BuildinfoPlugin)
            runTask(parent, "buildinfo")

        then:
            def json = ((File)parent.buildinfo.outputFile).readLines().join("\n")
            !json.contains("versionControl")
    }

    def "buildinfo file is cleaned in single module project"() {
        when:
            def parent = buildParentProject()
            parent.plugins.apply(BuildinfoPlugin)
            runTask(parent, "buildinfo")
            runTask(parent, "clean")

        then:
            !parent.buildinfo.outputFile.exists()
    }

    def "buildinfo file is created in custom location"() {
        when:
            def parent = buildParentProject()
            parent.plugins.apply(BuildinfoPlugin)
            parent.buildinfo {
                outputFile = new File(parent.buildDir, "otherfile.json")
            }
            runTask(parent, "buildinfo")

        then:
            parent.buildinfo.outputFile.exists()
    }

    private buildParentProject() {
        ProjectBuilder.builder()
                .withProjectDir(tmp.root)
                .build()
    }

    private buildChildProject(Project parent) {
        ProjectBuilder.builder()
                .withProjectDir(tmp.newFile("child"))
                .withParent(parent)
                .build()
    }

    private runTask(Project project, String taskName) {
        def task = project.tasks.getByName(taskName)
        task.actions.each {
            it.execute(task)
        }
    }
}
