package com.mreil.gradleplugins.buildinfo

import groovy.util.logging.Slf4j
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

@Slf4j
class BuildInfoFragmentProviderTest extends Specification {

    @Rule
    TemporaryFolder tmp = new TemporaryFolder()

    def "check all providers have names"() {
        when:
            def providers = findProviders()

        then:
            providers.size() > 0
            providers
                    .collect { provCl -> provCl.newInstance(tmp.root) }
                    .collect { prov ->
                log.warn("Found fragment provider: ${prov}")
                prov = prov as AbstractBuildInfoFragmentProvider
                assert prov.name != null
            }
    }

    Object findProviders() {
        return new ClassScanner<BuildInfoFragmentProvider>().findAll(BuildInfoFragmentProvider.class)
    }
}
