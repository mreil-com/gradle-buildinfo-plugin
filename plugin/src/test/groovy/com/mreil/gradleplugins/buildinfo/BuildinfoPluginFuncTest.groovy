package com.mreil.gradleplugins.buildinfo

import org.gradle.internal.impldep.org.apache.commons.io.FileUtils
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

import java.util.zip.ZipFile

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class BuildinfoPluginFuncTest extends Specification {

    @Rule
    TemporaryFolder tmp = new TemporaryFolder()

    @Shared
    public String buildGradle = """
plugins { id "com.mreil.buildinfo" }
"""

    @Shared
    public GradleRunner project

    def setup() {
        project = GradleRunner.create()
                .withProjectDir(tmp.root)
                .withPluginClasspath()
        tmp.newFile("build.gradle").write(buildGradle)
    }

    def "Plugin can be applied"() {
        when:
            def build = project
                    .withArguments("buildinfo", "--info", "--stacktrace")
                    .build()

        then:
            build.task(":buildinfo").outcome == SUCCESS
    }

    def "Java jar contains JSON"() {
        when:
            FileUtils.copyDirectory(new File("src/test/resources/multi-module-project"),
                    tmp.root)

            def build = GradleRunner.create()
                    .withProjectDir(tmp.root)
                    .withPluginClasspath()
                    .withArguments("jar", "--info")
                    .build()

        then:
            System.err.println(build.output)
            build.task(":java:jar").outcome == SUCCESS
            def f = new File(tmp.root, "java/build/libs/java-1.2.3-SNAPSHOT.jar")
            f.exists()
            new ZipFile(f).entries().collect {
                System.err.println(it)
                it
            }.find { it.name == "META-INF/buildinfo/buildinfo.json" }
    }
}
