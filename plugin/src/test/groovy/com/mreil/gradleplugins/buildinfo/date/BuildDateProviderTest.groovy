package com.mreil.gradleplugins.buildinfo.date

import spock.lang.Specification

class BuildDateProviderTest extends Specification {
    def "provides date"() {
        when:
            def date = new BuildDateProvider().get()

        then:
            date.present
    }
}
