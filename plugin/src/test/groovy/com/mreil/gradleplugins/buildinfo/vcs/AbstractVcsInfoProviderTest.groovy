package com.mreil.gradleplugins.buildinfo.vcs

import spock.lang.Specification

class AbstractVcsInfoProviderTest extends Specification {

    def "relative path is error"() {
        when:
            new TestProvider(new File("."))

        then:
            thrown IllegalArgumentException
    }

    def "non-existent path is error"() {
        when:
            new TestProvider(new File("/akjsdhkashjdkajsdhk"))

        then:
            thrown IllegalArgumentException
    }

    static class TestProvider extends AbstractVcsInfoProvider {
        protected TestProvider(File dir) {
            super(dir)
        }

        @Override
        String getType() {
            return "test"
        }

        @Override
        boolean validate() {
            return false
        }

        @Override
        protected Optional<VcsInfo> doGet() {
            return null
        }
    }
}
