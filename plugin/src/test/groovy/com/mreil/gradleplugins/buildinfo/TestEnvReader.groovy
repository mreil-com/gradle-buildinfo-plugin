package com.mreil.gradleplugins.buildinfo

import com.mreil.gradleplugins.buildinfo.util.EnvVariableReader

class TestEnvReader extends EnvVariableReader {
    TestEnvReader(Map<String, String> env) {
        super(env)
    }
}
