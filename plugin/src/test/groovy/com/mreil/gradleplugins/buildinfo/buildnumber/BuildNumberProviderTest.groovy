package com.mreil.gradleplugins.buildinfo.buildnumber

import com.mreil.gradleplugins.buildinfo.TestEnvReader
import spock.lang.Specification

class BuildNumberProviderTest extends Specification {

    def "find number"() {
        when:
            def bnp = new BuildNumberProvider(new TestEnvReader(["BUILD_NUMBER": "42"]))

        then:
            def buildNumber = bnp.get()
            buildNumber.present
            buildNumber.get() == 42
    }
}
