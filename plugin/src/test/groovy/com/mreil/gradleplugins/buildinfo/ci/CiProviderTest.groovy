package com.mreil.gradleplugins.buildinfo.ci

import com.mreil.gradleplugins.buildinfo.TestEnvReader
import spock.lang.Specification

class CiProviderTest extends Specification {

    def "find CI"() {
        when:
            def cip = new CiProvider(new TestEnvReader(["BUILD_NUMBER": "42"]))

        then:
            def isCi = cip.get()
            isCi.present
            isCi.get()
    }
}
