package com.mreil.gradleplugins.buildinfo.util

import spock.lang.Shared
import spock.lang.Specification

class EnvVariableReaderTest extends Specification {

    @Shared
    EnvVariableReader env = new EnvVariableReader([
            A: "VAL_A",
            B: "VAL_B",
            C: "VAL_C",
    ])

    def "find env variable"() {
        expect:
            def val = env.findFirst(["X", "B", "A"])
            val.present
            val.get() == "VAL_B"
    }

    def "not found"() {
        expect:
            def val = env.findFirst(["X", "Y", "Z"])
            !val.present
    }

}
