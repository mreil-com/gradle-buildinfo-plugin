package com.mreil.gradleplugins.buildinfo.vcs

import groovy.util.logging.Slf4j
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

@Slf4j
class VcsInfoProviderTest extends Specification {

    @Rule
    TemporaryFolder tmp = new TemporaryFolder()

    def "check all providers"() {
        when:
            def providers = VcsInfoProvider.findProviders()

        then:
            providers.size() > 0
            providers
                    .collect { provCl -> provCl.newInstance(tmp.root) }
                    .collect { prov ->
                log.info("Found fragment provider: ${prov}")
                prov = prov as AbstractVcsInfoProvider
                assert prov.name != null
                assert prov.type != null
            }
    }
}
